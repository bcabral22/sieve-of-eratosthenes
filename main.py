#Sieve of Eratosthenes: find all prime #'s up to n
#list all # upto n(starting from 2)
#identify first # in the list
#elimate all multiples of that #
#repeat 2 and 3 untile done (sqrt(n))
import math
N=10000
sqN= int(math.sqrt(N))
#print(sqN)
#will be the list for all numbers going to N
listUpToN=[]
#will be list for all multiples
multiples=[]
#fill the list
for i in range(1,N):
    listUpToN.append(i+1)

#print(listUpToN)
#for i in range(0,sqN):
index=0
#keeping doing this until it reaches the sqare root of 10000
while True:
    #this will be the number that divide into all the numbers in the list
    tester=listUpToN[index]
    #iterater through size of listUpTON
    for J in range(0,len(listUpToN)):
        #if value has no reamider to tester number then it is a multiple, do this if it is
        if listUpToN[J] % tester ==0:
            #add that multiple to the list of multiples
            multiples.append(listUpToN[J])
            #multiples.remove(listUpToN[index])
    #remove the intial multiple so it does not get removed        
    multiples.remove(listUpToN[index])
    #iterate through all values in mutlipeles list
    for pop in range(0,len(multiples)):
       #value in idex 
      remover=multiples[pop]
      # if value is in my big list of 10000 then do this
      if  remover in listUpToN:
          #reove the value from list
        listUpToN.remove(remover)
        ##listUpToN.pop()
  
    #once function reaches the sqrt or passes it then stop program
    if tester >=sqN:
        break
    #add one to index to move to mext value
    index=index+1
    #clear multiples so it doesnt keep checking for values that have already been checked and removed.
    multiples.clear()
#print(multiples)
#print new list
print(listUpToN)  
